import Vue from "vue";
import VueRouter from "vue-router";
import About from "../views/About.vue";
import MovieList from "../components/MovieList.vue";
import MovieDetail from "../components/MovieDetail.vue";
import MovieForm from "../components/MovieForm.vue";
import Login from "../components/Login.vue";
import NotFound from "@/views/NotFound.vue";
import auth from "@/common/auth";
import store from "@/common/store";
const user = store.state.user;

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: { public: true, isLoginPage: true }
  },
  {
    path: "/",
    redirect: "/movies"
  },
  {
    path: "/about",
    name: "About",
    component: About,
    meta: { public: true }
  },
  {
    path: "/movies",
    name: "MovieList",
    component: MovieList
  },
  {
    path: "/movies/new",
    name: "MovieCreate",
    component: MovieForm
  },
  {
    path: "/movies/:movieId",
    name: "MovieDetail",
    component: MovieDetail
  },
  {
    path: "/movies/:movieId/update",
    name: "MovieUpdate",
    component: MovieForm
  },
  {
    path: "*",
    name: "NotFound",
    component: NotFound
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  // Lo primero que hacemos antes de cargar ninguna ruta es comprobar si
  // el usuario está autenticado (revisando el token)
  auth.isAuthenticationChecked.finally(() => {
    // por defecto, el usuario debe estar autenticado para acceder a las rutas
    const requiresAuth = !to.meta.public;

    const requiredAuthority = to.meta.authority;
    const userIsLogged = user.logged;
    const loggedUserAuthority = user.authority;

    if (requiresAuth) {
      if (userIsLogged) {
        if (requiredAuthority && requiredAuthority != loggedUserAuthority) {
          // usuario logueado pero sin permisos
          alert(
            "Acceso no permitido para el usuario actual. Trate de autenticarse de nuevo"
          );
          auth.logout();
          next("/login");
        } else {
          // usuario logueado y con permisos adecuados
          next();
        }
      } else {
        // usuario no está logueado
        alert("Esta página requiere autenticación");
        next("/login");
      }
    } else {
      // página pública
      if (userIsLogged && to.meta.isLoginPage) {
        // si estamos logueados no hace falta volver a mostrar el login
        next({ name: "MovieList", replace: true });
      } else {
        next();
      }
    }
  });
});

export default router;
