export function getImageSrcGenerator(poster) {
  if (poster) {
    return function(movie) {
      if (!movie.hasImage) return "/placeholder_large.png";
      return `http://localhost:8080/api/movies/${movie.id}/image`;
    };
  } else {
    return function(movie) {
      if (!movie.hasImage) return "/placeholder_small.png";
      return `http://localhost:8080/api/movies/${movie.id}/image`;
    };
  }
}
