import HTTP from "@/common/http.js";

const RESOURCE = "movies";

export default {
  async getMovies() {
    return (await HTTP.get(RESOURCE)).data;
  },
  async getMovie(movieId) {
    const movie = (await HTTP.get(RESOURCE + "/" + movieId)).data;
    return movie;
  },
  async removeMovie(movieId) {
    return await HTTP.delete(RESOURCE + "/" + movieId);
  },
  async saveMovie(movie) {
    if (movie.id) {
      return (await HTTP.put(`${RESOURCE}/${movie.id}`, movie)).data;
    } else {
      return (await HTTP.post(`${RESOURCE}`, movie)).data;
    }
  },
  async saveImage(movieId, file) {
    const formData = new FormData();
    formData.append("file", file);
    return (
      await HTTP.post(`${RESOURCE}/${movieId}/image`, formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
    ).data;
  }
};
